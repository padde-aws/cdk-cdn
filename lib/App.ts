import cdk = require('@aws-cdk/cdk');
import s3 = require('@aws-cdk/aws-s3');
import cloudfront = require('@aws-cdk/aws-cloudfront');
import r53 = require('@aws-cdk/aws-route53');

export interface AppProps {
    RootDomainName: string;
    SubDomainName: string;
    HostedZoneID: string;
    S3WebsiteEndpoint: string;
    S3LogginBucket: string;
    CertificateArn: string;
}

export class App extends cdk.App {
    private _values: AppProps;
    public WWWBucket: s3.Bucket;
    public WebsiteCloudfront: cloudfront.CloudFrontWebDistribution;
    public DNSv4: r53.cloudformation.RecordSetResource;
    public DNSv6: r53.cloudformation.RecordSetResource;

    constructor(props: AppProps) {
        super();
        this._values = props;

        this.WWWBucket = new s3.Bucket(this, new cdk.FnJoin('.', [this._values.SubDomainName, this._values.RootDomainName]).toString(), {
            encryption: s3.BucketEncryption.S3Managed
        });

        this.WebsiteCloudfront = new cloudfront.CloudFrontWebDistribution(this,
            new cdk.FnJoin('.', [this._values.SubDomainName, this._values.RootDomainName, 's3.amazonaws.com']).toString(), {
                enableIpV6: true,
                aliasConfiguration: {
                    names: [new cdk.FnJoin('.', [this._values.SubDomainName, this._values.RootDomainName]).toString()],
                    acmCertRef: this._values.CertificateArn,
                    sslMethod: cloudfront.SSLMethod.SNI,
                    securityPolicy: cloudfront.SecurityPolicyProtocol.TLSv1_2_2018
                },
                originConfigs: [
                    {
                        s3OriginSource: {
                            s3BucketSource: this.WWWBucket
                        },
                        behaviors: [ {
                            isDefaultBehavior: true
                        }]
                    }
                ],
                httpVersion: cloudfront.HttpVersion.HTTP2,
                priceClass: cloudfront.PriceClass.PriceClass100,
                errorConfigurations: [{
                    errorCode: 404,
                    responseCode: 200,
                    responsePagePath: 'index.html',
                    errorCachingMinTtl: 30
                }],
                defaultRootObject: 'index.html',
                viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.RedirectToHTTPS
            }
        );

        this.DNSv4 = new r53.cloudformation.RecordSetResource(this,
            new cdk.FnJoin('.', [this._values.SubDomainName, this._values.RootDomainName]).toString(), {
                hostedZoneId: this._values.RootDomainName + '.',
                name: new cdk.FnJoin('.', [this._values.SubDomainName, this._values.RootDomainName]).toString(),
                type: 'A',
                aliasTarget: {
                    hostedZoneId: this._values.HostedZoneID + '.',
                    dnsName: this.WebsiteCloudfront.domainName
                }
        });


        this.DNSv6 = new r53.cloudformation.RecordSetResource(this,
            new cdk.FnJoin('.', [this._values.SubDomainName, this._values.RootDomainName]).toString(), {
                hostedZoneId: this._values.RootDomainName + '.',
                name: new cdk.FnJoin('.', [this._values.SubDomainName, this._values.RootDomainName]).toString(),
                type: 'A',
                aliasTarget: {
                    hostedZoneId: this._values.HostedZoneID + '.',
                    dnsName: this.WebsiteCloudfront.domainName
                }
        });
    }
}
